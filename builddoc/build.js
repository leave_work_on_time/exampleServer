
const cache=require('./cache');
const path=require('path');
cache();

function resolve(dir) {
  return path.join(__dirname, '..', dir)
}
const curDir = resolve('.') //当前路径

var webpackProcess = spawn(process.platform === 'win32' ? 'npm.cmd' : 'npm', ['run', 'build','--prefix',curDir]);

// 捕获标准输出并将其打印到控制台 
webpackProcess.stdout.on('data', function (data) {
  console.log(`${data}`);
});

// 捕获标准错误输出并将其打印到控制台 
webpackProcess.stderr.on('data', function (data) {
  console.log(`${data}`);
});

// 注册子进程关闭事件 
webpackProcess.on('exit', function (code, signal) {
  console.log('child process eixt ,exit:' + code);
});