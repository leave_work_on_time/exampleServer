/*
 * @Author: XiaoFeng Chen 
 * @Date: 2018-10-13 00:11:07 
 * @Last Modified by: XiaoFeng Chen
 * @Last Modified time: 2018-10-25 17:47:42
 * 緩存文件
 */
const path = require('path');
const fs = require('fs')
const fser = require('./fser')
const docRender = require('./docer').render



//配置检查
// E:\html\leave_work_on_time\exampleServer-demo\README.md E:\html\leave_work_on_time\exampleServer-demo\exampleServer

function resolve(dir) {
  return path.join(__dirname, '..', dir)
}

const parDir = resolve('..'); // 父级目录 根目录
const cachePath = resolve('./.cache'); //缓存目录
const curDir = resolve('.') //当前路径

function resolveDaddyPath(dir) {
  return path.join(parDir, dir)
}
//读取配置
const config = require(resolveDaddyPath('./.example.js'))

module.exports=function(){
    
    
//清理cache
if (fs.existsSync(cachePath)) {
    fser.clearDir(cachePath)
  }
  fs.mkdirSync(cachePath)
  
  //当前目录
  var cdir = resolve('.').split('\\')
  var ignoreDirectoryPattern = new RegExp(`node_modules|${cdir[cdir.length - 1]}|^\\..|dist|package.json|package-lock.json`);
  var pfiles = fs.readdirSync(parDir)
  // console.log(pfiles);
  pfiles.forEach((file) => {
    if (!ignoreDirectoryPattern.test(file)) {
      var filepath = path.resolve(parDir, file)
      var npath = path.resolve(cachePath, file);
      //拷贝到cache
      // log(filepath,npath)
      fser.copy(filepath, npath, {
        ignoreDirectoryPattern
      })
    }
  })
  docRender();
}
