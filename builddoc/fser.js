/*
 * @Author: XiaoFeng Chen 
 * @Date: 2018-10-12 13:38:37 
 * @Last Modified by: XiaoFeng Chen
 * @Last Modified time: 2018-11-28 20:42:29
 * 文件的增删改查
 */

//@ts-check
const fs = require('fs-extra'); //https://github.com/jprichardson/node-fs-extra
const stat = fs.statSync;
const path = require('path');
// fs.mkdirSync('E:\\html\\leave_work_on_time\\exampleServer-demo\\exampleServer\\.cache\\src\\basic');
// fs.mkdirSync('E:/html/leave_work_on_time/exampleServer-demo/exampleServer/.cache/src/basic');
/**
 * 拷贝目录  
 * @param {String} src 原路径 
 * @param {String} dst 目标路径
 */
function copy(src, dst,options) {
  src = src.replace(/\\/g, '/');
  dst = dst.replace(/\\/g, '/');

  var st = stat(src);
  if (st.isDirectory()) {
    if (!fs.existsSync(dst)) {
      fs.mkdirSync(dst)
    }
    copyDir(src, dst,options)
  } else {
    // var routes=src.split('/');
    // dst+=('/'+routes[routes.length-1]);
    var readable = fs.readFileSync(src); //创建读取流
    fs.writeFileSync(dst, readable); //创建写入流
  }

  function copyDir(src, dst,options) {
    //读取目录
    var files = fs.readdirSync(src)
    // console.log(files)
    files.forEach(function (path) {

      if(options.ignoreDirectoryPattern.test(path)){
        return;
      }
      var _src = src + '/' + path;
      var _dst = dst + '/' + path;
      var readable;
      var writable;
      var st = stat(_src)

      if (st.isFile()) {
        readable = fs.readFileSync(_src); //创建读取流
        fs.writeFileSync(_dst, readable); //创建写入流
        // readable.pipe(writable);
      } else if (st.isDirectory()) {
        //测试某个路径下文件是否存在
        var exists = fs.existsSync(_dst)
        if (exists) { //存在
          copyDir(_src, _dst,options);
        } else { //不存在
          fs.mkdirSync(_dst)
          copyDir(_src, _dst,options)
        }
      }
    });
  }

}

// function copyInto(src,dst){
//   src=src.replace(/\\/g,'/');
//   dst=dst.replace(/\\/g,'/');

//     var st=stat(src);
//     if(st.isDirectory()){
//         copyDir(src, dst)
//     }else{
//         var routes=src.split('\\');
//         dst+=('\\'+routes[routes.length-1]);
//         var readable = fs.createReadStream(src); //创建读取流
//         var writable = fs.createWriteStream(dst); //创建写入流
//         readable.pipe(writable);
//     }
// }

// https://juejin.im/post/5ab32b20518825557f00d36c
/**
 * 移除文件、文件夹
 * @param {String} filepath 移除文件的路径
 */
function remove(filepath) {
  if (stat(filepath).isDirectory()) {
    removeDir(filepath)
  } else {
    //删除文件
    fs.unlinkSync(filepath);
  }

  //移除文件夹
  function removeDir(filepath) {
    // console.log(filepath)
    let files = fs.readdirSync(filepath)
    files.forEach(file=>{
      let newPath = path.join(filepath,file);
      if (stat(newPath).isDirectory()) {
        //如果是文件夹就递归下去
        removeDir(newPath);
      } else {
        //删除文件
        fs.unlinkSync(newPath);
      }
    })
    // console.log('================='+fs.readdirSync(filepath))
    fs.rmdirSync(filepath) //如果文件夹是空的，就将自己删除掉  
    // console.log(filepath)
  }
  // function _removeDir
}
/**
 * 清空目录
 * @param {String} dir 目录路径
 */
function clearDir(dir) {
  let files = fs.readdirSync(dir)
  for (var i = files.length - 1; i > -1; i--) {
    let newPath = path.join(dir, files[i]);
    if (stat(newPath).isDirectory()) {
      //如果是文件夹就递归下去
      remove(newPath);
    } else {
      //删除文件
      fs.unlinkSync(newPath);
    }
  }
}



// copy('E:\\html\\leave_work_on_time\\exampleServer-demo\\src','E:\\html\\leave_work_on_time\\exampleServer-demo\\exampleServer\\.cache\\src')
module.exports = {
  copy,
  remove:fs.removeSync,
  clearDir:fs.removeSync
}
