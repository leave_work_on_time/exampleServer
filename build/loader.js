// const loaderUtils = require('loader-utils')
// const path = require('path')
const fs = require('fs')

module.exports = function(source){
  //http://www.css88.com/doc/webpack/api/loaders/
  // console.log(this.resourcePath);

  if(this.resourcePath.indexOf('\\docs')!==-1){
    let  __resource=fs.readFileSync(this.resourcePath,'UTF-8')


    source=source.replace(/export[\s]+default[\s]+(.*)/,function(str,ex){
      ex=ex.trim()
      return `
        ${ex}.__resource=\`${__resource}\`
        \n
        export default ${ex}`
    });
  }
  return source
}
