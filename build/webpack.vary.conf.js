const HtmlWebpackPlugin = require('html-webpack-plugin')
const path=require('path');

const exampleConfig = require(path.join(__dirname, '..', '../.example.js'));

console.log(exampleConfig)

let entry={ app: './src/main.js'}
entry[exampleConfig.name]=path.resolve(__dirname, '../','./.cache/src/index.js');
if(exampleConfig.others){
  exampleConfig.others.forEach(element => {
    entry[element.name]=path.resolve(__dirname, '../','./.cache/src/'+element.path);
  });
}
module.exports={
  context: path.resolve(__dirname, '../'),
  entry,
  plugins:[
        // generate dist index.html with correct asset hash for caching.
    // you can customize output by editing /index.html
    // see https://github.com/ampedandwired/html-webpack-plugin
    new HtmlWebpackPlugin({
      // filename: config.build.index,
      filename: 'index.html',
      template: 'index.html',
      inject: true,
      chunksSortMode(a,b){
        var order=[exampleConfig.name,'app']
        return order.indexOf(a.names[0])>order.indexOf(b.names[0]);
      }
      // minify: {
      //   removeComments: true,
      //   collapseWhitespace: true,
      //   removeAttributeQuotes: true
      //   // more options:
      //   // https://github.com/kangax/html-minifier#options-quick-reference
      // },
      // // necessary to consistently work with multiple chunks via CommonsChunkPlugin
      // chunksSortMode: 'dependency'
    }),
  ]
}