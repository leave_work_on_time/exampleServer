module.exports=function(source){
    this.cacheable();
    var reg=/\sfill=(\S)+\s/g,
        fills=source.match(reg);
    return fills&&fills.length>1?source:source.replace(/\sfill=(\S)+\s/g,'');
}