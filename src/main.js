// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import App from './App'
// import router from './router'
import './components'

import components from '../.cache/.doc.pages.js'

import '../.cache/src/index'

var routes={
  pagename:'app-content',
  children:[]
}

for (const i in components ) {
  if (components.hasOwnProperty(i)) {
    const element = components[i];
    routes.children.push({
      pagename: i,
      component: element
    })
  }
}

Vue.config.productionTip = false;

Vue.prototype.$rxRouter.init(routes)


/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { 
    App
  },
  template: '<App/>'
})
